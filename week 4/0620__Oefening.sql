use aptunes;
create index VoornaamFamilienaamIdx
on muzikanten(Voornaam, Familienaam);
show index from muzikanten;