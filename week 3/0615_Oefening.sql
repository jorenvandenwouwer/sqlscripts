use modernways;

alter view AuteursBoeken
as
select concat(personen.Voornaam,  ' ' , personen.Familienaam) as Auteur , boeken.titel, boeken.id
from personen
inner join publicaties on personen.Id = publicaties.Personen_Id
inner join boeken  on boeken.Id = publicaties.Boeken_Id;
select *
from auteursboeken;
