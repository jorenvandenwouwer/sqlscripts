use ModernWays;
drop table if exists Personen;
# de nieuwe tabel personen maken 
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);
# tabel van personen vullen met gegevens van boeken
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;
#de andere kolommen toevoegen aan de tabel Personen
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);
# kolom toevoegen die de forgein key wordt
alter table Boeken add Personen_Id int null;
# foreigkey kolom invullen met de data van boeken
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
	where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
#not null constaint toevoegen aangezien dit een foreign key is
alter table Boeken change Personen_Id Personen_Id int not null;
#overbodige kolom verwijderen uit boeken aangezien die nu bij personen zit
alter table Boeken drop column Voornaam,
    drop column Familienaam;
# de kolom boeken foreign key maken
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);



