insert into Boeken (
   Titel,
   verschijningsdatum,
   uitgeverij,
   Personen_ID
)
values (
   'De Woorden',
   '1961',
   'De Bezige Bij',
   (select Id from Personen where Familienaam = 'Sartre' and Voornaam = 'Jean-Paul')
);
