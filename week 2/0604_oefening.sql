use modernways;
drop table uitleningen;
CREATE TABLE Uitleningen (
BeginDatum DATE NOT NULL,
EindDatum DATE,
Boeken_Id INT NOT NULL,
Leden_Id INT NOT NULL,
CONSTRAINT fk_Uitleningen_Boeken FOREIGN KEY (Boeken_Id) references Boeken(Id),
CONSTRAINT fk_Uitleningen_Leden FOREIGN KEY (Leden_Id) references Leden(Id)
);